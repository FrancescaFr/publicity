% Outreach 2014 Slide Deck
% Portland State Aerospace Society (PSAS)


Oregon's Space Program
--------------------------------------------------------------------------------

Help us make rockets

![Aerospace project at PSU. Open to everyone!](images/PSAS_group.jpg)


Why Rockets?
--------------------------------------------------------------------------------

 - Labs are safe and predictable.
 - Rockets can kill you! And are hard!
 - Okay, so let's build rockets.
 - Our goal: **Put a 1 kg nanosatellite into orbit.**


Plan Of Attack
--------------------------------------------------------------------------------

 1. Build steerable rocket.
 1. Get lawyers.
 1. ???
 1. Orbit!


--------------------------------------------------------------------------------

An aside about rocket physics...


--------------------------------------------------------------------------------

![](images/launch.jpg)


--------------------------------------------------------------------------------

<video class="stretch" data-autoplay src="videos/launch_hill.webm"></video>

